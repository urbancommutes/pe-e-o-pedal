package com.urbancommutes.server.bike.data;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.prefs.CsvPreference;

import com.google.gson.Gson;
import com.urbancommutes.shared.bean.Station;

public class BS {

	private BS() {
		//
	}

	public static void main(String[] args) throws Exception {
		final List<Station> l = getStations();
		// final Gson gson = new Gson();
		// for (Station s : l) {
		// System.out.println(gson.toJson(s));
		// }
		System.out.println(new Gson().toJson(l));
	}

	public static List<Station> getStations() throws Exception {
		final String HEADER = "lat,lng,idEstacao,nome,endereco,referencia,descTipo,codArea,statusOnline,statusOperacao,numBicicletas,vagasOcupadas,ocorrenciasAbertas,taxaOcupacao\n";
		List<Station> stations = new ArrayList<>();
		List<String> wp = viewSource("http://ww2.mobilicidade.com.br/bikesampa/mapaestacao.asp");
		StringBuilder builder = new StringBuilder(HEADER);
		StringBuilder stationB = new StringBuilder();
		for (String s : wp) {
			if (s.contains("var point = new GLatLng(")) {
				stationB.append(s.substring("var point = new GLatLng(".length(), s.length() - 2));
				stationB.append(", ");
			}
			if (s.contains("map.addOverlay( criaPonto(")) {
				String substring = s.substring("map.addOverlay( criaPonto(point,".length(), s.length() - 4);
				stationB.append(substring + "\n");
				String csv = stationB.toString().replace("'", "\"");
				if (count(csv) > 14) {
					int lastIndexOf = stationB.lastIndexOf(",");
					stationB.replace(lastIndexOf, lastIndexOf + 1, ".");
				}
				builder.append(stationB.toString().replace("'", "\""));
				stationB = new StringBuilder();
			}
		}
		try (ICsvBeanReader beanReader = new CsvBeanReader(new StringReader(builder.toString()),
				CsvPreference.STANDARD_PREFERENCE)) {
			final String[] header = beanReader.getHeader(true);
			Station station;
			while ((station = beanReader.read(Station.class, header)) != null) {
				stations.add(station);
			}
			return stations;
		}
	}

	private static int count(final String s) throws Exception {
		try (ICsvListReader listReader = new CsvListReader(new StringReader(s), CsvPreference.STANDARD_PREFERENCE);) {
			List<String> values = listReader.read();
			// System.out.println(String.format("lineNo=%s, rowNo=%s, values=%s",
			// listReader.getLineNumber(), listReader.getRowNumber(), values));
			return values.size();
		}
	}

	private static List<String> viewSource(final String url) throws Exception {
		List<String> page = new ArrayList<>();
		URL _url = new URL(url);
		try (BufferedReader in = new BufferedReader(new InputStreamReader(_url.openStream()))) {
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				page.add(inputLine);
			}
		}
		return page;
	}
}
