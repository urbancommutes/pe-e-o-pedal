package com.urbancommutes.server.service;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.urbancommutes.client.service.DataService;
import com.urbancommutes.server.bike.data.BikeData;
import com.urbancommutes.shared.bean.Bicicletarios;
import com.urbancommutes.shared.bean.Infra;
import com.urbancommutes.shared.bean.InfraTemp;
import com.urbancommutes.shared.bean.Latlng;
import com.urbancommutes.shared.bean.Place;
import com.urbancommutes.shared.bean.SharedRoutes;
import com.urbancommutes.shared.bean.Station;
import com.urbancommutes.shared.bean.Tip;

public class DataServiceImpl extends RemoteServiceServlet implements DataService {

	private static final long serialVersionUID = -2239783417228578529L;

	@Override
	public Station[] getBikeSampaStations() throws Exception {
		List<Station> stationsAux = BikeData.getStations();
		Station[] stations = new Station[stationsAux.size()];
		for (int i = 0; i < stationsAux.size(); i++) {
			stations[i] = stationsAux.get(i);
		}
		return stations;
	}

	@Override
	public InfraTemp[] getTemporaryRoutes() throws Exception {
		return BikeData.getTempRoutes();
	}

	@Override
	public void setTemporaryRoutes(ArrayList<Latlng> position) throws Exception {

		/*for (Latlng latLng : position) {
			System.out.println("Lat: " + latLng.getLat());
			System.out.println("Long: " + latLng.getLng());

		}*/
	}

	@Override
	public Bicicletarios[] getBicicletarios() throws Exception {
		return BikeData.getBicicletarios();
	}

	@Override
	public Infra[] getRoutes() throws Exception {
		return BikeData.getRoutes();
	}

	@Override
	public InfraTemp[] getTempRoutes() throws Exception {
		return BikeData.getTempRoutes();
	}

	@Override
	public SharedRoutes[] getSharedRoutes() throws Exception {
		return BikeData.getSharedRoutes();
	}

	@Override
	public Place[] getPlaces() throws Exception {
		return BikeData.getPlaces();
	}

	@Override
	public Tip[] getTips() throws Exception {
		return BikeData.getTips();
	}
}
