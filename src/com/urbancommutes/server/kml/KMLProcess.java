package com.urbancommutes.server.kml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.urbancommutes.shared.bean.Bicicletarios;
import com.urbancommutes.shared.bean.Infra;
import com.urbancommutes.shared.bean.InfraTemp;
import com.urbancommutes.shared.bean.Latlng;

import de.micromata.opengis.kml.v_2_2_0.Coordinate;
import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.Feature;
import de.micromata.opengis.kml.v_2_2_0.IconStyle;
import de.micromata.opengis.kml.v_2_2_0.Kml;
import de.micromata.opengis.kml.v_2_2_0.LineString;
import de.micromata.opengis.kml.v_2_2_0.LineStyle;
import de.micromata.opengis.kml.v_2_2_0.Placemark;
import de.micromata.opengis.kml.v_2_2_0.Point;
import de.micromata.opengis.kml.v_2_2_0.Style;
import de.micromata.opengis.kml.v_2_2_0.StyleSelector;

@SuppressWarnings("unused")
public class KMLProcess {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		File f = new File(".");
		final String PATH = (String) f.getAbsolutePath().subSequence(0, f.getAbsolutePath().length() - 1)
				+ "src/com/urbancommutes/server/kml/";
		System.out.println(System.getProperty("user.dir"));
		// /Users/viniscius/Documents/workspace/peeopedal
		System.out.println(processCiclofaixaOperacionaldeLazer());
	}

	public static String processCiclofaixaOperacionaldeLazer() {
		List<InfraTemp> infra = new ArrayList<>();

		Kml kml = Kml.unmarshal(new File(System.getProperty("user.dir") + "/kml/CiclofaixaOperacionaldeLazer.kml"),
				false);
		final Document document = (Document) kml.getFeature();
		List<Feature> t = document.getFeature();
		for (Object o : t) {
			final Placemark placemark = (Placemark) o;
			LineString line = (LineString) placemark.getGeometry();
			List<Coordinate> coordinates = line.getCoordinates();
			String name = placemark.getName();
			String descr = placemark.getDescription();
			List<Latlng> routeTemp = new ArrayList<>();
			for (Coordinate c : coordinates) {
				routeTemp.add(new Latlng(c.getLatitude() + "", c.getLongitude() + ""));
			}
			Latlng[] route = new Latlng[routeTemp.size()];
			for (int i = 0; i < routeTemp.size(); i++) {
				route[i] = routeTemp.get(i);
			}
			InfraTemp infratemp = new InfraTemp(name, descr, route);
			infra.add(infratemp);
		}
		return new Gson().toJson(infra);
	}

	public static String processBicicletrioseParaciclosIntegradosaoSistemadeTransportedaCidadedeSoPaulo() {
		List<Bicicletarios> b = new ArrayList<>();
		Kml kml = Kml.unmarshal(new File(System.getProperty("user.dir")
				+ "/kml/BicicletrioseParaciclosIntegradosaoSistemadeTransportedaCidadedeSoPaulo.kml"), false);
		final Document document = (Document) kml.getFeature();
		HashMap<String, String> style = new HashMap<>();
		// get styles
		List<StyleSelector> styles = document.getStyleSelector();
		for (StyleSelector s : styles) {
			String id = s.getId();
			Style ss = (Style) s;
			IconStyle iconStyle = ss.getIconStyle();
			String href = iconStyle.getIcon().getHref();
			style.put("#" + id, href);
		}

		List<Feature> t = document.getFeature();
		for (Object o : t) {
			final Placemark placemark = (Placemark) o;
			Point point = (Point) placemark.getGeometry();
			List<Coordinate> coordinates = point.getCoordinates();
			String name = placemark.getName();
			String description = placemark.getDescription();
			description = description.substring("<div dir=\"ltr\">".length());
			String icon = style.get(placemark.getStyleUrl());

			Latlng[] route = new Latlng[coordinates.size()];
			int count = 0;
			for (Coordinate c : coordinates) {
				double lat = c.getLatitude();
				double lng = c.getLongitude();
				route[count++] = new Latlng(lat + "", lng + "");
			}
			b.add(new Bicicletarios(name, description, icon, route));
		}
		return new Gson().toJson(b);

	}

	public static String processInfraestruturaCicloviriaPermanentedaCidadedeSoPaulo() {
		List<Infra> in = new ArrayList<>();

		Kml kml = Kml.unmarshal(new File(System.getProperty("user.dir")
				+ "/kml/InfraestruturaCicloviriaPermanentedaCidadedeSoPaulo.kml"), false);
		final Document document = (Document) kml.getFeature();
		HashMap<String, String> style = new HashMap<>();
		List<StyleSelector> styles = document.getStyleSelector();
		for (StyleSelector s : styles) {
			String id = s.getId();
			Style ss = (Style) s;
			LineStyle lineStyle = ss.getLineStyle();
			String color = lineStyle.getColor().substring(0, 6);
			if(color.equals("FFFFFF")){
				color = "4C0099";
			}else if(color.equals("FFFF55")){
				color = "3333FF";
			}else if(color.equals("FF0000")){
				color = "00CCCC";
			}
			style.put("#" + id, "#" + color);
		}
		List<Feature> t = document.getFeature();
		for (Object o : t) {
			final Placemark placemark = (Placemark) o;
			LineString line = (LineString) placemark.getGeometry();
			List<Coordinate> coordinates = line.getCoordinates();
			String name = placemark.getName();
			String descr = placemark.getDescription();
			String color = style.get(placemark.getStyleUrl());

			Latlng[] route = new Latlng[coordinates.size()];
			int count = 0;
			for (Coordinate c : coordinates) {
				double lat = c.getLatitude();
				double lng = c.getLongitude();
				route[count++] = new Latlng(lat + "", lng + "");
			}
			in.add(new Infra(name, descr, color, route));
		}
		return new Gson().toJson(in);
	}
}
