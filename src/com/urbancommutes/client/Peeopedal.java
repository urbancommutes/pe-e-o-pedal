package com.urbancommutes.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.urbancommutes.client.home.Home;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Peeopedal implements EntryPoint {

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		RootLayoutPanel.get().add(new Home());
	}
}
