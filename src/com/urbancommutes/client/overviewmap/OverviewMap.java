package com.urbancommutes.client.overviewmap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.maps.client.base.LatLng;
import com.google.gwt.maps.client.base.Size;
import com.google.gwt.maps.client.overlays.Marker;
import com.google.gwt.maps.client.overlays.MarkerImage;
import com.google.gwt.maps.client.overlays.MarkerOptions;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.urbancommutes.client.home.Home;
import com.urbancommutes.client.mainscreen.MainScreen;
import com.urbancommutes.client.maps.BicicletariosMap;
import com.urbancommutes.client.maps.BikeSharingMap;
import com.urbancommutes.client.maps.Map;
import com.urbancommutes.client.maps.RoutesMap;
import com.urbancommutes.client.maps.TempRoutesMap;
import com.urbancommutes.client.resources.Resources;

public class OverviewMap extends Composite {

	private static OverviewMapUiBinder uiBinder = GWT.create(OverviewMapUiBinder.class);

	Resources res = GWT.create(Resources.class);

	interface OverviewMapUiBinder extends UiBinder<Widget, OverviewMap> {
	}

	@UiField
	FlowPanel mapContainer;

	@UiField
	Button back;

	private Map map;

	public OverviewMap() {
		initWidget(uiBinder.createAndBindUi(this));

		back.getElement().appendChild(new Image(res.back()).getElement());
		back.getElement().getStyle().setCursor(Cursor.POINTER);

		map = new Map();
		if (Home.coord != null) {
			MarkerOptions options = MarkerOptions.newInstance();
			options.setPosition(LatLng.newInstance(Home.coord.getLatitude(), Home.coord.getLongitude()));
			options.setMap(map.getMap());
			MarkerImage icon = MarkerImage.newInstance("http://maps.google.com/mapfiles/arrow.png");
			icon.setScaledSize(Size.newInstance(25, 25));
			options.setIcon(icon);
			Marker marker = Marker.newInstance(options);
			marker.setMap(map.getMap());
		}

		mapContainer.add(map);
		BicicletariosMap.loadBicicletariosMap(map);
		BikeSharingMap.loadBikeSharingMap(map);
		RoutesMap.loadRoutesMap(map);
		TempRoutesMap.loadTempRoutesMap(map);

		map.triggerResize();
	}

	@UiHandler("back")
	public void getBackClickHandler(ClickEvent event) {
		Home.switchView(new MainScreen(), "Pé e o Pedal");
	}

}
