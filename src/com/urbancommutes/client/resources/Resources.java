package com.urbancommutes.client.resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface Resources extends ClientBundle {

	@Source("shared.png")
	ImageResource shared();

	@Source("places.png")
	ImageResource places();

	@Source("bikesampa.png")
	ImageResource bikesampa();

	@Source("tips.png")
	ImageResource tips();

	@Source("settings.png")
	ImageResource settings();

	@Source("maps.png")
	ImageResource maps();

	@Source("back.png")
	ImageResource back();

	@Source("start.png")
	ImageResource start();

	@Source("stop.png")
	ImageResource stop();

	@Source("shower.png")
	ImageResource shower();
	
	@Source("helmet.png")
	ImageResource helmet();
	
	@Source("bike-park.png")
	ImageResource bikePark();
	
	@Source("star.png")
	ImageResource star();

	@Source("add.png")
	ImageResource add();
}
