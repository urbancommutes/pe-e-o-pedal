package com.urbancommutes.client.mainscreen;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.urbancommutes.client.bikesharing.BikeSharing;
import com.urbancommutes.client.home.Home;
import com.urbancommutes.client.overviewmap.OverviewMap;
import com.urbancommutes.client.places.Places;
import com.urbancommutes.client.resources.Resources;
import com.urbancommutes.client.shareroute.ShareRoute;
import com.urbancommutes.client.tips.Tips;

public class MainScreen extends Composite {

	private static MainScreenUiBinder uiBinder = GWT.create(MainScreenUiBinder.class);
	Resources res = GWT.create(Resources.class);

	interface MainScreenUiBinder extends UiBinder<Widget, MainScreen> {
	}

	@UiField
	Button shareRoute;

	@UiField
	Button places;

	@UiField
	Button bikeSharing;

	@UiField
	Button tips;

	@UiField
	Button settings;

	@UiField
	Button maps;

	public MainScreen() {
		initWidget(uiBinder.createAndBindUi(this));
		shareRoute.getElement().appendChild(new Image(res.shared()).getElement());
		shareRoute.getElement().getStyle().setCursor(Cursor.POINTER);
		places.getElement().appendChild(new Image(res.places()).getElement());
		places.getElement().getStyle().setCursor(Cursor.POINTER);
		bikeSharing.getElement().appendChild(new Image(res.bikesampa()).getElement());
		bikeSharing.getElement().getStyle().setCursor(Cursor.POINTER);
		tips.getElement().appendChild(new Image(res.tips()).getElement());
		tips.getElement().getStyle().setCursor(Cursor.POINTER);
		settings.getElement().appendChild(new Image(res.settings()).getElement());
		settings.getElement().getStyle().setCursor(Cursor.POINTER);
		maps.getElement().appendChild(new Image(res.maps()).getElement());
		maps.getElement().getStyle().setCursor(Cursor.POINTER);
	}

	@UiHandler("shareRoute")
	public void getShareRouteClickHandler(ClickEvent event) {
		Home.switchView(new ShareRoute(), "Rotas Compartilhadas");
	}

	@UiHandler("places")
	public void getPlacesClickHandler(ClickEvent event) {
		Home.switchView(new Places(), "Lugares");
	}

	@UiHandler("bikeSharing")
	public void getBikeSharingClickHandler(ClickEvent event) {
		Home.switchView(new BikeSharing(), "Bicicleta compartilhada");
	}

	@UiHandler("tips")
	public void getTipsClickHandler(ClickEvent event) {
		Home.switchView(new Tips(), "Dicas");
	}

	@UiHandler("settings")
	public void getSettingsClickHandler(ClickEvent event) {
		final DialogBox box = new DialogBox(true);
		box.setText("Desenvolvido por Urban Commutes");
		box.add(new Label("Contato: urbancommutes@urbancommutes.com"));
		box.center();
	}

	@UiHandler("maps")
	public void getMapsClickHandler(ClickEvent event) {
		Home.switchView(new OverviewMap(), "Mapa");
	}
}
