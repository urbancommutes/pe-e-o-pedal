package com.urbancommutes.client.home;

import java.util.ArrayList;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.geolocation.client.Geolocation;
import com.google.gwt.geolocation.client.Position;
import com.google.gwt.geolocation.client.Position.Coordinates;
import com.google.gwt.geolocation.client.PositionError;
import com.google.gwt.maps.client.LoadApi;
import com.google.gwt.maps.client.LoadApi.LoadLibrary;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.urbancommutes.client.mainscreen.MainScreen;

public class Home extends Composite {

	private static HomeUiBinder uiBinder = GWT.create(HomeUiBinder.class);

	interface HomeUiBinder extends UiBinder<Widget, Home> {
	}

	@UiField
	static HTMLPanel main;

	@UiField
	static Label title;

	public static Coordinates coord = null;

	public Home() {
		initWidget(uiBinder.createAndBindUi(this));

		loadMapApi();
		Geolocation.getIfSupported().getCurrentPosition(new Callback<Position, PositionError>() {
			@Override
			public void onSuccess(Position result) {
				coord = result.getCoordinates();
				loadMainScreen();
			}

			@Override
			public void onFailure(PositionError reason) {
				coord = null;
				reason.printStackTrace();
			}
		});
	}

	public static void switchView(IsWidget newView, String title) {
		main.clear();
		main.add(newView);
		if (title != null) {
			Home.title.setText(title);
		}
	}

	private void loadMapApi() {
		boolean sensor = true;
		ArrayList<LoadLibrary> loadLibraries = new ArrayList<LoadApi.LoadLibrary>();
		// loadLibraries.add(LoadLibrary.ADSENSE);
		loadLibraries.add(LoadLibrary.DRAWING);
		// loadLibraries.add(LoadLibrary.GEOMETRY);
		// loadLibraries.add(LoadLibrary.PANORAMIO);
		// loadLibraries.add(LoadLibrary.PLACES);
		// loadLibraries.add(LoadLibrary.WEATHER);
		// loadLibraries.add(LoadLibrary.VISUALIZATION);
		Runnable onLoad = new Runnable() {
			@Override
			public void run() {
			}
		};
		LoadApi.go(onLoad, loadLibraries, sensor);
	}

	private void loadMainScreen() {
		switchView(new MainScreen(), "Pé e o Pedal");
	}
}
