package com.urbancommutes.client.shareroute;

import java.util.ArrayList;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.geolocation.client.Geolocation;
import com.google.gwt.geolocation.client.Position;
import com.google.gwt.geolocation.client.Position.Coordinates;
import com.google.gwt.geolocation.client.PositionError;
import com.google.gwt.maps.client.base.LatLng;
import com.google.gwt.maps.client.base.Size;
import com.google.gwt.maps.client.overlays.Marker;
import com.google.gwt.maps.client.overlays.MarkerImage;
import com.google.gwt.maps.client.overlays.MarkerOptions;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.urbancommutes.client.home.Home;
import com.urbancommutes.client.mainscreen.MainScreen;
import com.urbancommutes.client.maps.Map;
import com.urbancommutes.client.maps.SharedRoutesMap;
import com.urbancommutes.client.resources.Resources;
import com.urbancommutes.shared.bean.Latlng;

public class ShareRoute extends Composite {

	private static ShareRouteUiBinder uiBinder = GWT.create(ShareRouteUiBinder.class);
	Resources res = GWT.create(Resources.class);

	interface ShareRouteUiBinder extends UiBinder<Widget, ShareRoute> {
	}

	@UiField
	FlowPanel mapContainer;

	@UiField
	Button back;

	@UiField
	Button startStop;

	private Coordinates coord = null;
	private boolean isRouteStarted = false;
	private ArrayList<Latlng> route;
	private Timer timer;
	private Map map;
	private Image image = null;

	public ShareRoute() {
		initWidget(uiBinder.createAndBindUi(this));

		back.getElement().appendChild(new Image(res.back()).getElement());
		back.getElement().getStyle().setCursor(Cursor.POINTER);

		image = new Image(res.start());
		startStop.getElement().appendChild(image.getElement());
		startStop.getElement().getStyle().setCursor(Cursor.POINTER);

		map = new Map();
		if (Home.coord != null) {
			MarkerOptions options = MarkerOptions.newInstance();
			options.setPosition(LatLng.newInstance(Home.coord.getLatitude(), Home.coord.getLongitude()));
			options.setMap(map.getMap());
			MarkerImage icon = MarkerImage.newInstance("http://maps.google.com/mapfiles/arrow.png");
			icon.setScaledSize(Size.newInstance(25, 25));
			options.setIcon(icon);
			Marker marker = Marker.newInstance(options);
			marker.setMap(map.getMap());
		}

		mapContainer.add(map);
		map.triggerResize();
	}

	private void setRoute() {
		timer = new Timer() {
			@Override
			public void run() {
				Geolocation.getIfSupported().getCurrentPosition(new Callback<Position, PositionError>() {
					@Override
					public void onSuccess(Position result) {

						coord = result.getCoordinates();
						Latlng position = new Latlng();
						position.setLat(String.valueOf(coord.getLatitude()));
						position.setLng(String.valueOf(coord.getLongitude()));
						route.add(position);
					}

					@Override
					public void onFailure(PositionError reason) {
						coord = null;
						reason.printStackTrace();
					}
				});
			}
		};
		timer.scheduleRepeating(5000);
	}

	private void finishRoute() {
		SharedRoutesMap.loadSharedRoutesMap(map);
		timer.cancel();
	}

	@UiHandler("back")
	public void getBackClickHandler(ClickEvent event) {
		Home.switchView(new MainScreen(), "Pé e o Pedal");
	}

	@UiHandler("startStop")
	public void getStartStopClickHandler(ClickEvent event) {
		if (!isRouteStarted) {
			route = new ArrayList<Latlng>();
			isRouteStarted = true;
			startStop.getElement().removeChild(image.getElement());
			image = new Image(res.stop());
			startStop.getElement().appendChild(image.getElement());
			setRoute();
		} else {
			startStop.getElement().removeChild(image.getElement());
			image = new Image(res.start());
			startStop.getElement().appendChild(image.getElement());
			finishRoute();
		}
	}
}
