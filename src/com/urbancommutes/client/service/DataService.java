package com.urbancommutes.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.urbancommutes.shared.bean.Bicicletarios;
import com.urbancommutes.shared.bean.Infra;
import com.urbancommutes.shared.bean.InfraTemp;
import com.urbancommutes.shared.bean.Latlng;
import com.urbancommutes.shared.bean.Place;
import com.urbancommutes.shared.bean.SharedRoutes;
import com.urbancommutes.shared.bean.Station;
import com.urbancommutes.shared.bean.Tip;

@RemoteServiceRelativePath("serviceData")
public interface DataService extends RemoteService {

	Station[] getBikeSampaStations() throws Exception;

	InfraTemp[] getTemporaryRoutes() throws Exception;

	void setTemporaryRoutes(ArrayList<Latlng> position) throws Exception;

	Bicicletarios[] getBicicletarios() throws Exception;

	Infra[] getRoutes() throws Exception;

	InfraTemp[] getTempRoutes() throws Exception;
	
	SharedRoutes[] getSharedRoutes() throws Exception;
	
	Place[] getPlaces() throws Exception;
	
	Tip[] getTips() throws Exception;
}
