package com.urbancommutes.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.urbancommutes.shared.bean.Bicicletarios;
import com.urbancommutes.shared.bean.Infra;
import com.urbancommutes.shared.bean.InfraTemp;
import com.urbancommutes.shared.bean.Latlng;
import com.urbancommutes.shared.bean.Place;
import com.urbancommutes.shared.bean.SharedRoutes;
import com.urbancommutes.shared.bean.Station;
import com.urbancommutes.shared.bean.Tip;

public interface DataServiceAsync {

	void getTemporaryRoutes(AsyncCallback<InfraTemp[]> callback);

	void getBikeSampaStations(AsyncCallback<Station[]> callback);

	void setTemporaryRoutes(ArrayList<Latlng> position, AsyncCallback<Void> callback);

	void getBicicletarios(AsyncCallback<Bicicletarios[]> callback);

	void getRoutes(AsyncCallback<Infra[]> callback);

	void getTempRoutes(AsyncCallback<InfraTemp[]> callback);

	void getSharedRoutes(AsyncCallback<SharedRoutes[]> callback);

	void getPlaces(AsyncCallback<Place[]> callback);

	void getTips(AsyncCallback<Tip[]> callback);
	
}
