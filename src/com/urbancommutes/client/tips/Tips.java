package com.urbancommutes.client.tips;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.urbancommutes.client.home.Home;
import com.urbancommutes.client.mainscreen.MainScreen;
import com.urbancommutes.client.resources.Resources;
import com.urbancommutes.client.service.DataService;
import com.urbancommutes.client.service.DataServiceAsync;
import com.urbancommutes.shared.bean.Tip;

public class Tips extends Composite {

	private static TipsUiBinder uiBinder = GWT.create(TipsUiBinder.class);

	Resources res = GWT.create(Resources.class);

	interface TipsUiBinder extends UiBinder<Widget, Tips> {
	}

	@UiField
	Button back;

	@UiField
	VerticalPanel vp;

	private int countRow;

	public Tips() {
		initWidget(uiBinder.createAndBindUi(this));
		back.getElement().appendChild(new Image(res.back()).getElement());
		back.getElement().getStyle().setCursor(Cursor.POINTER);
		searchTips();
	}

	private void searchTips() {
		DataServiceAsync service = GWT.create(DataService.class);
		service.getTips(new AsyncCallback<Tip[]>() {

			@Override
			public void onSuccess(Tip[] result) {

				for (Tip tip : result) {
					HorizontalPanel row = new HorizontalPanel();
					row.getElement().getStyle().setPadding(4, Unit.PCT);
					row.getElement().getStyle().setWidth(100, Unit.PCT);
					if (countRow++ % 2 == 0) {
						row.getElement().getStyle().setBackgroundColor("#F0F0F0");
					}
					
					VerticalPanel tipsInfo = new VerticalPanel();
					tipsInfo.getElement().getStyle().setWidth(100, Unit.PCT);

					Label tipsTitle = new Label(tip.getDesc());
					tipsTitle.getElement().getStyle().setFontWeight(FontWeight.BOLD);
					tipsTitle.getElement().getStyle().setFontSize(120, Unit.PCT);
					tipsTitle.getElement().getStyle().setTextAlign(TextAlign.CENTER);
					
					tipsInfo.add(tipsTitle);
					row.add(tipsInfo);

					vp.getElement().appendChild(row.getElement());
				}
			}

			@Override
			public void onFailure(Throwable caught) {
			}
		});
	}

	@UiHandler("back")
	public void getBackClickHandler(ClickEvent event) {
		Home.switchView(new MainScreen(), "Pé e o Pedal");
	}
}
