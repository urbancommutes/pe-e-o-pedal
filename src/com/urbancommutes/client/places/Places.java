package com.urbancommutes.client.places;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.TextAlign;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.urbancommutes.client.home.Home;
import com.urbancommutes.client.mainscreen.MainScreen;
import com.urbancommutes.client.resources.Resources;
import com.urbancommutes.client.service.DataService;
import com.urbancommutes.client.service.DataServiceAsync;
import com.urbancommutes.shared.bean.Place;

public class Places extends Composite {

	private static PlacesUiBinder uiBinder = GWT.create(PlacesUiBinder.class);

	Resources res = GWT.create(Resources.class);

	interface PlacesUiBinder extends UiBinder<Widget, Places> {
	}

	@UiField
	Button back;
	@UiField
	Button add;

	@UiField
	VerticalPanel vp;

	private int countRow;

	public Places() {
		initWidget(uiBinder.createAndBindUi(this));
		back.getElement().appendChild(new Image(res.back()).getElement());
		back.getElement().getStyle().setCursor(Cursor.POINTER);
		add.getElement().appendChild(new Image(res.add()).getElement());
		add.getElement().getStyle().setCursor(Cursor.POINTER);
		searchPlaces();
	}

	private void searchPlaces() {
		DataServiceAsync service = GWT.create(DataService.class);
		service.getPlaces(new AsyncCallback<Place[]>() {

			@Override
			public void onSuccess(Place[] result) {

				for (Place place : result) {
					HorizontalPanel row = new HorizontalPanel();
					row.getElement().getStyle().setWidth(100, Unit.PCT);
					if (countRow++ % 2 == 0) {
						row.getElement().getStyle().setBackgroundColor("#F0F0F0");
					}

					HorizontalPanel images = new HorizontalPanel();
					images.getElement().getStyle().setWidth(20, Unit.PCT);

					Image helmet = new Image(res.bikePark());
					images.add(helmet);
					Image shower = new Image(res.shower());
					images.add(shower);

					if (place.getHasBikeParking().equalsIgnoreCase("false")) {
						helmet.getElement().getStyle().setOpacity(0);
					}

					if (place.getHasShower().equalsIgnoreCase("false")) {
						shower.getElement().getStyle().setOpacity(0);
					}

					VerticalPanel placeInfo = new VerticalPanel();
					placeInfo.getElement().getStyle().setWidth(70, Unit.PCT);

					Label placeName = new Label(place.getPlaceName());
					placeName.getElement().getStyle().setFontWeight(FontWeight.BOLD);
					placeName.getElement().getStyle().setFontSize(110, Unit.PCT);
					placeName.getElement().getStyle().setTextAlign(TextAlign.CENTER);

					Label placeAddress = new Label(place.getPlaceAddress());
					placeAddress.getElement().getStyle().setFontWeight(FontWeight.LIGHTER);
					placeAddress.getElement().getStyle().setFontSize(50, Unit.PCT);
					placeAddress.getElement().getStyle().setTextAlign(TextAlign.CENTER);

					HorizontalPanel starsPanel = new HorizontalPanel();
					starsPanel.getElement().getStyle().setWidth(10, Unit.PCT);

					int rate = Integer.valueOf(place.getRate());

					for (int i = 0; i < 5; i++) {
						Image star = new Image(res.star());
						if (i > rate)
							star.getElement().getStyle().setOpacity(0);
						starsPanel.add(star);
					}

					placeInfo.add(placeName);
					placeInfo.add(placeAddress);

					row.add(images);
					row.add(placeInfo);
					row.add(starsPanel);

					vp.getElement().appendChild(row.getElement());
				}
			}

			@Override
			public void onFailure(Throwable caught) {
			}
		});
	}

	@UiHandler("back")
	public void getBackClickHandler(ClickEvent event) {
		Home.switchView(new MainScreen(), "Lugares");
	}
}
