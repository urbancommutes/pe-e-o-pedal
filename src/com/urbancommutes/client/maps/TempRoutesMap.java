package com.urbancommutes.client.maps;

import com.google.gwt.ajaxloader.client.ArrayHelper;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.maps.client.base.LatLng;
import com.google.gwt.maps.client.events.MouseEvent;
import com.google.gwt.maps.client.events.click.ClickMapEvent;
import com.google.gwt.maps.client.events.click.ClickMapHandler;
import com.google.gwt.maps.client.overlays.InfoWindow;
import com.google.gwt.maps.client.overlays.InfoWindowOptions;
import com.google.gwt.maps.client.overlays.Polyline;
import com.google.gwt.maps.client.overlays.PolylineOptions;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.urbancommutes.client.service.DataService;
import com.urbancommutes.client.service.DataServiceAsync;
import com.urbancommutes.shared.bean.InfraTemp;
import com.urbancommutes.shared.bean.Latlng;

public class TempRoutesMap {

	public static void loadTempRoutesMap(final Map map) {
		DataServiceAsync service = GWT.create(DataService.class);
		service.getTemporaryRoutes(new AsyncCallback<InfraTemp[]>() {

			@Override
			public void onSuccess(InfraTemp[] result) {
				for (final InfraTemp in : result) {
					int count = 0;
					LatLng[] ll = new LatLng[in.getRoute().length];
					for (Latlng all : in.getRoute()) {
						ll[count++] = LatLng.newInstance(Double.parseDouble(all.getLat()),
								Double.parseDouble(all.getLng()));
					}
					JsArray<LatLng> simpleLatLngArr = ArrayHelper.toJsArray(ll);
					PolylineOptions opts = PolylineOptions.newInstance();
					// opts.setMap(mapWidget); // you can attach it to the map
					// here
					opts.setPath(simpleLatLngArr);
					opts.setStrokeColor("#00FF00");
					opts.setStrokeOpacity(0.8);
					opts.setStrokeWeight(5);

					final Polyline polyLine = Polyline.newInstance(opts);
					polyLine.setMap(map.getMap());

					polyLine.addClickHandler(new ClickMapHandler() {

						@Override
						public void onEvent(ClickMapEvent event) {
							drawInfoWindow(polyLine, event.getMouseEvent(), map, in);
						}
					});

				}
			}

			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}
		});
	}

	private static void drawInfoWindow(Polyline polyl, MouseEvent mouseEvent, Map map, InfraTemp in) {
		if (polyl == null || mouseEvent == null) {
			return;
		}

		// HTML html = new HTML("You clicked on: " +
		// mouseEvent.getLatLng().getToString());
		HTML html = new HTML(in.getName() + "<br>" + in.getDescr());

		InfoWindowOptions options = InfoWindowOptions.newInstance();
		options.setContent(html);
		options.setPosition(LatLng.newInstance(mouseEvent.getLatLng().getLatitude(), mouseEvent.getLatLng()
				.getLongitude()));
		InfoWindow iw = InfoWindow.newInstance(options);
		iw.open(map.getMap());
	}
}