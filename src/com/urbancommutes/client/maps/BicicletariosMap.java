package com.urbancommutes.client.maps;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.maps.client.base.LatLng;
import com.google.gwt.maps.client.base.Size;
import com.google.gwt.maps.client.events.MouseEvent;
import com.google.gwt.maps.client.events.click.ClickMapEvent;
import com.google.gwt.maps.client.events.click.ClickMapHandler;
import com.google.gwt.maps.client.overlays.InfoWindow;
import com.google.gwt.maps.client.overlays.InfoWindowOptions;
import com.google.gwt.maps.client.overlays.Marker;
import com.google.gwt.maps.client.overlays.MarkerImage;
import com.google.gwt.maps.client.overlays.MarkerOptions;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.urbancommutes.client.service.DataService;
import com.urbancommutes.client.service.DataServiceAsync;
import com.urbancommutes.shared.bean.Bicicletarios;
import com.urbancommutes.shared.bean.Latlng;

public class BicicletariosMap {

	public static void loadBicicletariosMap(final Map map) {
		DataServiceAsync service = GWT.create(DataService.class);
		service.getBicicletarios(new AsyncCallback<Bicicletarios[]>() {

			@Override
			public void onSuccess(Bicicletarios[] result) {
				final Size size = Size.newInstance(25, 25);
				final MarkerImage subway = MarkerImage
						.newInstance("http://maps.gstatic.com/mapfiles/ms2/micons/subway.png");
				subway.setScaledSize(size);
				final MarkerImage bus = MarkerImage.newInstance("http://maps.gstatic.com/mapfiles/ms2/micons/bus.png");
				bus.setScaledSize(size);
				final MarkerImage rail = MarkerImage
						.newInstance("http://maps.gstatic.com/mapfiles/ms2/micons/rail.png");
				rail.setScaledSize(size);

				for (final Bicicletarios b : result) {
					Latlng[] ll = b.getRoute();
					LatLng center = LatLng.newInstance(Double.parseDouble(ll[0].getLat()),
							Double.parseDouble(ll[0].getLng()));
					MarkerOptions options = MarkerOptions.newInstance();
					options.setPosition(center);
					options.setTitle(b.getName() + "\n" + b.getDescr());
					if (b.getName().contains("CPTM")) {
						options.setIcon(rail);
					} else if (b.getName().contains("SPTrans") || b.getName().contains("EMTU")) {
						options.setIcon(bus);
					} else {
						options.setIcon(subway);
					}
					final Marker markerBasic = Marker.newInstance(options);
					markerBasic.setMap(map.getMap());

					markerBasic.addClickHandler(new ClickMapHandler() {
						@Override
						public void onEvent(ClickMapEvent event) {
							drawInfoWindow(markerBasic, event.getMouseEvent(), map, b);
						}
					});
				}

			}

			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}
		});
	}

	private static void drawInfoWindow(Marker marker, MouseEvent mouseEvent, Map map, Bicicletarios b) {
		if (marker == null || mouseEvent == null) {
			return;
		}

		// HTML html = new HTML("You clicked on: " +
		// mouseEvent.getLatLng().getToString());
		HTML html = new HTML(b.getName() + "<br>" + b.getDescr());

		InfoWindowOptions options = InfoWindowOptions.newInstance();
		options.setContent(html);
		InfoWindow iw = InfoWindow.newInstance(options);
		iw.open(map.getMap(), marker);
	}
}
