package com.urbancommutes.client.maps;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.maps.client.base.LatLng;
import com.google.gwt.maps.client.base.Size;
import com.google.gwt.maps.client.events.MouseEvent;
import com.google.gwt.maps.client.events.click.ClickMapEvent;
import com.google.gwt.maps.client.events.click.ClickMapHandler;
import com.google.gwt.maps.client.overlays.InfoWindow;
import com.google.gwt.maps.client.overlays.InfoWindowOptions;
import com.google.gwt.maps.client.overlays.Marker;
import com.google.gwt.maps.client.overlays.MarkerImage;
import com.google.gwt.maps.client.overlays.MarkerOptions;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.urbancommutes.client.service.DataService;
import com.urbancommutes.client.service.DataServiceAsync;
import com.urbancommutes.shared.bean.Station;

public class BikeSharingMap {

	public static void loadBikeSharingMap(final Map map) {
		DataServiceAsync service = GWT.create(DataService.class);
		service.getBikeSampaStations(new AsyncCallback<Station[]>() {

			@Override
			public void onSuccess(Station[] result) {
				final Size size = Size.newInstance(25, 25);
				final MarkerImage cinza = MarkerImage
						.newInstance("http://ww2.mobilicidade.com.br/bikesampa/img/icone-estacoes-cinza.gif");
				cinza.setScaledSize(size);
				final MarkerImage normal = MarkerImage
						.newInstance("http://ww2.mobilicidade.com.br/bikesampa/img/icone-estacoes.gif");
				normal.setScaledSize(size);
				final MarkerImage desativado = MarkerImage
						.newInstance("http://ww2.mobilicidade.com.br/bikesampa/img/icone-estacoes-desativado.gif");
				desativado.setScaledSize(size);
				final MarkerImage vazia = MarkerImage
						.newInstance("http://ww2.mobilicidade.com.br/bikesampa/img/icone-estacoes-vazia.gif");
				vazia.setScaledSize(size);
				final MarkerImage cheia = MarkerImage
						.newInstance("http://ww2.mobilicidade.com.br/bikesampa/img/icone-estacoes-cheia.gif");
				cheia.setScaledSize(size);
				// img.setAnchor(Point.newInstance(6, 20));
				for (final Station s : result) {
					LatLng center = LatLng.newInstance(Double.parseDouble(s.getLat()), Double.parseDouble(s.getLng()));
					MarkerOptions options = MarkerOptions.newInstance();
					options.setPosition(center);
					options.setTitle(s.getNome());

					if (s.getStatusOperacao().equals("EO") && s.getStatusOnline().equals("A")) {
						options.setIcon(normal);
						if (s.getTaxaOcupacao().equals("0")) {
							options.setIcon(vazia);
						} else if (s.getTaxaOcupacao().equals("100")) {
							options.setIcon(cheia);
						}
					} else if (s.getStatusOperacao().equals("EI")) {
						if (s.getStatusOnline().equals("A") || s.getStatusOnline().equals("I")) {
							options.setIcon(cinza);
						} else {
							options.setIcon(desativado);
						}
					} else if (s.getStatusOperacao().equals("EM")
							|| (s.getStatusOnline().equals("I") && s.getStatusOperacao().equals("EO"))) {
						options.setIcon(desativado);
					}

					final Marker markerBasic = Marker.newInstance(options);
					markerBasic.setMap(map.getMap());

					markerBasic.addClickHandler(new ClickMapHandler() {
						@Override
						public void onEvent(ClickMapEvent event) {
							drawInfoWindow(markerBasic, event.getMouseEvent(), map, s);
						}
					});
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				caught.printStackTrace();
			}
		});
	}

	private static void drawInfoWindow(Marker marker, MouseEvent mouseEvent, Map map, Station s) {
		if (marker == null || mouseEvent == null) {
			return;
		}

		// HTML html = new HTML("You clicked on: " +
		// mouseEvent.getLatLng().getToString());
		String spots = s.getNumBicicletas();
		String bikes = s.getVagasOcupadas();
		HTML html = new HTML(s.getNome() + "<br>" + s.getEndereco() + "<br>Bikes: " + bikes + "<br>Vagas Livres: "
				+ (Integer.parseInt(spots) - Integer.parseInt(bikes)));

		InfoWindowOptions options = InfoWindowOptions.newInstance();
		options.setContent(html);
		InfoWindow iw = InfoWindow.newInstance(options);
		iw.open(map.getMap(), marker);
	}

}
