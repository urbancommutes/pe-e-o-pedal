package com.urbancommutes.client.maps;

import com.google.gwt.ajaxloader.client.ArrayHelper;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.maps.client.base.LatLng;
import com.google.gwt.maps.client.overlays.Polyline;
import com.google.gwt.maps.client.overlays.PolylineOptions;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.urbancommutes.client.service.DataService;
import com.urbancommutes.client.service.DataServiceAsync;
import com.urbancommutes.shared.bean.Latlng;
import com.urbancommutes.shared.bean.SharedRoutes;

public class SharedRoutesMap {
	
	public static void loadSharedRoutesMap(final Map map) {
		
		DataServiceAsync service = GWT.create(DataService.class);
		service.getSharedRoutes(new AsyncCallback<SharedRoutes[]>() {
			
			@Override
			public void onSuccess(SharedRoutes[] result) {
				for (final SharedRoutes s : result) {
					int count = 0;
					LatLng[] ll = new LatLng[s.getRoute().length];
					for (Latlng all : s.getRoute()) {
						ll[count++] = LatLng.newInstance(Double.parseDouble(all.getLat()), Double.parseDouble(all.getLng()));
					}
					
					JsArray<LatLng> simpleLatLngArr = ArrayHelper.toJsArray(ll);
					PolylineOptions opts = PolylineOptions.newInstance();
					opts.setPath(simpleLatLngArr);
					opts.setStrokeColor("#FF0000");
					opts.setStrokeOpacity(0.8);
					opts.setStrokeWeight(5);

					Polyline polyLine = Polyline.newInstance(opts);
					polyLine.setMap(map.getMap());
				}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}

}
