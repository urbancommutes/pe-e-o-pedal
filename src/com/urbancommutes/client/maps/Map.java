package com.urbancommutes.client.maps;

import com.google.gwt.core.client.GWT;
import com.google.gwt.maps.client.MapOptions;
import com.google.gwt.maps.client.MapTypeId;
import com.google.gwt.maps.client.MapWidget;
import com.google.gwt.maps.client.base.LatLng;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.urbancommutes.client.home.Home;

public class Map extends Composite {

	private static MapUiBinder uiBinder = GWT.create(MapUiBinder.class);

	interface MapUiBinder extends UiBinder<Widget, Map> {
	}

	@UiField
	FlowPanel container;

	private MapWidget map;
	private static boolean hasCenter = false;

	public Map() {
		initWidget(uiBinder.createAndBindUi(this));

		LatLng center = null;
		if (Home.coord != null) {
			center = LatLng.newInstance(Home.coord.getLatitude(), Home.coord.getLongitude());
			hasCenter = true;
		}

		MapOptions options = MapOptions.newInstance();
		options.setZoom(14);
		options.setCenter(center);
		options.setMapTypeId(MapTypeId.ROADMAP);

		map = new MapWidget(options);
		container.clear();
		container.add(map);
		map.setSize("100%", "100%");
	}

	public void triggerResize() {
		final Timer timer = new Timer() {
			@Override
			public void run() {
				map.triggerResize();
			}
		};
		timer.scheduleRepeating(250);

		Timer cancelTimer = new Timer() {
			@Override
			public void run() {
				timer.cancel();
			}
		};
		cancelTimer.schedule(3000);
	}

	public MapWidget getMap() {
		return map;
	}

	public static boolean hasCenter() {
		return hasCenter;
	}
}
