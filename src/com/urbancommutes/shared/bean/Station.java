package com.urbancommutes.shared.bean;

import java.io.Serializable;

public class Station implements Serializable {

	private static final long serialVersionUID = 8875551462496381691L;

	private String lat;
	private String lng;
	private String idEstacao;
	private String nome;
	private String endereco;
	private String referencia;
	private String descTipo;
	private String codArea;
	private String statusOnline;
	private String statusOperacao;
	private String numBicicletas;
	private String vagasOcupadas;
	private String ocorrenciasAbertas;
	private String taxaOcupacao;

	public Station() {
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getIdEstacao() {
		return idEstacao;
	}

	public void setIdEstacao(String idEstacao) {
		this.idEstacao = idEstacao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getDescTipo() {
		return descTipo;
	}

	public void setDescTipo(String descTipo) {
		this.descTipo = descTipo;
	}

	public String getCodArea() {
		return codArea;
	}

	public void setCodArea(String codArea) {
		this.codArea = codArea;
	}

	public String getStatusOnline() {
		return statusOnline;
	}

	public void setStatusOnline(String statusOnline) {
		this.statusOnline = statusOnline;
	}

	public String getStatusOperacao() {
		return statusOperacao;
	}

	public void setStatusOperacao(String statusOperacao) {
		this.statusOperacao = statusOperacao;
	}

	public String getNumBicicletas() {
		return numBicicletas;
	}

	public void setNumBicicletas(String numBicicletas) {
		this.numBicicletas = numBicicletas;
	}

	public String getVagasOcupadas() {
		return vagasOcupadas;
	}

	public void setVagasOcupadas(String vagasOcupadas) {
		this.vagasOcupadas = vagasOcupadas;
	}

	public String getOcorrenciasAbertas() {
		return ocorrenciasAbertas;
	}

	public void setOcorrenciasAbertas(String ocorrenciasAbertas) {
		this.ocorrenciasAbertas = ocorrenciasAbertas;
	}

	public String getTaxaOcupacao() {
		return taxaOcupacao;
	}

	public void setTaxaOcupacao(String taxaOcupacao) {
		this.taxaOcupacao = taxaOcupacao;
	}
}