package com.urbancommutes.shared.bean;

import java.io.Serializable;

public class Place implements Serializable {

	private static final long serialVersionUID = 2618102525959649391L;
	
	private String id;
	private String hasBikePark;
	private String hasShower;
	private String placeName;
	private String placeAddress;
	private String rate;
	
	public Place(){}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getHasBikeParking() {
		return hasBikePark;
	}
	public void setHasBikeParking(String hasBikeParking) {
		this.hasBikePark = hasBikeParking;
	}
	public String getHasShower() {
		return hasShower;
	}
	public void setHasShower(String hasShower) {
		this.hasShower = hasShower;
	}
	public String getPlaceName() {
		return placeName;
	}
	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}
	public String getPlaceAddress() {
		return placeAddress;
	}
	public void setPlaceAddress(String placeAddress) {
		this.placeAddress = placeAddress;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	
}
