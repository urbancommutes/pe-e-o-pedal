package com.urbancommutes.shared.bean;

import java.io.Serializable;

public class Latlng implements Serializable {

	private static final long serialVersionUID = -1609883412939630814L;

	private String lat;
	private String lng;

	public Latlng() {
		// Default
	}

	public Latlng(String lat, String lng) {
		super();
		this.lat = lat;
		this.lng = lng;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

}
