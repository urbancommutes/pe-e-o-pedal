package com.urbancommutes.shared.bean;

import java.io.Serializable;

public class Tip implements Serializable {

private static final long serialVersionUID = 2618102525959649391L;
	
	private String id;
	private String desc;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
}
