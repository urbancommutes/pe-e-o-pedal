package com.urbancommutes.shared.bean;

import java.io.Serializable;

public class SharedRoutes implements Serializable {

	private static final long serialVersionUID = 2618102525959649391L;
	private String id;
	private Latlng[] route;

	public SharedRoutes() {
		// default
	}

	public SharedRoutes(String id, Latlng[] route) {
		super();
		this.id = id;
		this.route = route;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Latlng[] getRoute() {
		return route;
	}

	public void setRoute(Latlng[] route) {
		this.route = route;
	}


}
