package com.urbancommutes.shared.bean;

import java.io.Serializable;

public class InfraTemp implements Serializable {

	private static final long serialVersionUID = -6002122057424872625L;

	private String name;
	private String descr;
	private Latlng[] route;

	public InfraTemp() {
		// default
	}

	public InfraTemp(String name, String descr, Latlng[] route) {
		super();
		this.name = name;
		this.descr = descr;
		this.route = route;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public Latlng[] getRoute() {
		return route;
	}

	public void setRoute(Latlng[] route) {
		this.route = route;
	}

}
