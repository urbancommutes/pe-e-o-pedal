package com.urbancommutes.shared.bean;

import java.io.Serializable;

public class Bicicletarios implements Serializable {

	private static final long serialVersionUID = 2618102525959649391L;
	private String name;
	private String descr;
	private String icon;
	private Latlng[] route;

	public Bicicletarios() {
		// default
	}

	public Bicicletarios(String name, String descr, String icon, Latlng[] route) {
		super();
		this.name = name;
		this.descr = descr;
		this.icon = icon;
		this.route = route;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Latlng[] getRoute() {
		return route;
	}

	public void setRoute(Latlng[] route) {
		this.route = route;
	}

}
