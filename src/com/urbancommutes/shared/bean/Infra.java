package com.urbancommutes.shared.bean;

import java.io.Serializable;

public class Infra implements Serializable {

	private static final long serialVersionUID = 3454428950727388543L;

	private String name;
	private String descr;
	private String color;
	private Latlng[] route;

	public Infra() {
		// default
	}

	public Infra(String nome, String descr, String color, Latlng[] route) {
		super();
		this.name = nome;
		this.descr = descr;
		this.color = color;
		this.route = route;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Latlng[] getRoute() {
		return route;
	}

	public void setRoute(Latlng[] route) {
		this.route = route;
	}

}
